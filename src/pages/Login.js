import React from 'react'
import { useNavigate } from 'react-router-dom';

export const Login = ({authenticate}) => {
    const navigate = useNavigate();
    const loginHandler = () => {
        authenticate();
        navigate('/profile');
    }
  return (
    <div>
        <h2>Plase login</h2>
        <button onClick={loginHandler}>Login</button>
    </div>
  )
}

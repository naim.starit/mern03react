import React, {useState} from 'react';

const UseStateHook = () => {
const [myName, setMyName] = useState('Next');
const [age, setAge] = useState(10);
const [count, setCount] = useState(0);
const [info, setInfo] = useState({firstName: '', lastName: ''});
const [values, setValues] = useState([10, 20, 30])

   const changeAgeHandler = () => {
       console.log("I am working...............");
       setAge(20);
       setMyName("Topper")
    }
  
    const incrementHandler = () => {
      for(var i=1; i<=10; i++) {
        setCount(prevState => prevState + 1)
      }
    }

    const addValueHandler = () => {
      let value = Math.floor(Math.random()*100) + 50;
      setValues([...values, value])
    }
const {firstName, lastName} = info;
  return (
    <div>
      <h1>useState hook</h1>
      <h1>Values</h1> 
      <button onClick={addValueHandler}>Add new value</button>
      {
        values.map((ele, ind)=>(
          <h2 key={ind}>{ele}</h2>
        ))
      }
      <h1>
        {myName} - {age}
      </h1>
      <button onClick={changeAgeHandler}>Change Age</button>
      <h1>count - {count}</h1>
      <button onClick={incrementHandler}>Increment by 10</button>
      <h1>My name info</h1>
      <h2>
        First name: {firstName} - Last Name: {lastName}
      </h2>
      <label>First Name</label>
      <input
        type="text"
        value={firstName}
        onChange={(e) => setInfo({...info, firstName: e.target.value })}
      />
      <label>Last Name</label>
      <input
        type="text"
        value={lastName}
        onChange={(e) => setInfo({...info, lastName: e.target.value })}
      />
    </div>
  );
}

export default UseStateHook;
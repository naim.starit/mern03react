import React from 'react';
const Hello = ({name, age, children}) => {
    // const {name, age} = props; // alternative way
    return (
      <div>
        <h1>Hello - {name ? name : "Next Topper"}</h1>
        <br />
        <h1>Your age is - {age ? age : "0"}</h1>
        {children}
      </div>
    );
}

export default Hello; 


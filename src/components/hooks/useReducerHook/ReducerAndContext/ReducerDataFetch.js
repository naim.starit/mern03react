import axios from 'axios';
import React, {useReducer, useEffect} from 'react';
const initialState = {
    loading: true,
    data: {},
    error: ''
}

const reducer = (state, action) => {
    switch(action.type) {
        case 'success':
            return { loading: false, data: action.payload, error: ''};
        case 'failure':
            return { loading: false, data: {}, error: action.payload};
        default:
             return state;
    }
}

export const ReducerDataFetch = () => {
    const [info, dispatch] = useReducer(reducer, initialState);
  
    useEffect(()=>{
        axios.get('https://jsonplaceholder.typicode.com/users/1')
        .then(result => {
            console.log(result.data);
            dispatch({type: 'success', payload: result.data})
        })
        .catch(err => {
            dispatch({type: 'failure', payload: err})
        })
  }, [])
    return (
      <div>
        {info.loading ? (
          <h1>..............Loading............</h1>
        ) : (
          <>
            <h1>User Info</h1>
            <h1>Name : {info.data.name}</h1>
            <h1>Email : {info.data.email}</h1>
          </>
        )}
      </div>
    );
}

import { render } from '@testing-library/react';
import React, { Component } from 'react'

class UpdatingComp extends Component {
    constructor(){
        super();
        this.state = {
            count: 0
        }
        console.log('Constructor');
    }

    updateHandler = () => {
        this.setState({
            count: 5
        })
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        console.log("from shouldComponentUpdate");
        return true;
    };

    componentDidUpdate() {
        console.log("from componentDidUpdate");
    }

  render() {
      console.log('from render');
    return (
      <div>
          <h1>{this.state.count}</h1>
          <button onClick={this.updateHandler}>update</button>
      </div>
    )
  }
}

export default UpdatingComp;

// componentWillReceiveProps() // X
// shouldComponentUpdate(nextProps, nextState)
// componentWillUpdate(nextProps, nextState); // X
// render()
// componentDidUpdate()
import axios from 'axios';
import React, {useEffect, useState} from 'react'
import { ShowCoordinate } from './ShowCoordinate';

export const UseEffectHook = () => {
    const [loading, setLoading] = useState(true);
    const [id, setId] = useState(1);
    const [data, setData] = useState({});
    const [error, setError] = useState('');
    const [show, setShow] = useState(true);
    
    useEffect(() => {
      //  axios.get("https://jsonplaceholder.typicode.com/users")
      //  .then(result=> {
      //       setData(result.data);
      //       setLoading(false)
      //  })
      //  .catch(err=>{
      //    setError(err)
      //      setLoading(false);
      //  })

      async function getData() {
        let result = await axios.get(
          `https://jsonplaceholder.typicode.com/users/${id}`
        );
        setData(result.data);
        setLoading(false);
      }
      getData();
    }, [id])



  return (
    <div>
      {loading ? <h1>.........Loading.......</h1> : 
      (
          <>
          {show && <ShowCoordinate />}
          <button onClick={()=>setShow(false)}>Hide Coordinate</button>
          <h1>{id}</h1>
          <button onClick={()=>setId(parseInt(id)+1)}>Change ID</button>
          <input type='number' value={id} onChange={(e)=>setId(e.target.value)} />
          <h2>name: {data.name}</h2>
          <h3>email: {data.email}</h3>
            {/* {
                data.map(ele=> (
                    <h2 key={ele.id}> name: {ele.name} </h2>
                ))
            } */}
          </>
      )
      }
    </div>
  );
}

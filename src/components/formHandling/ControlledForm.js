import React, { Component } from 'react'
import {divisionList} from '../ListRendering/data';

class ControlledForm extends Component {
  constructor() {
    super();
    this.state = {
      fullName: "",
      age: "",
      myClass: '',
      division: '',
      skills: [],
    };
  }

  changeHandler = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
   checkboxHandler = (event) => {
    //  this.setState({
    //    skills: [...this.state.skills, event.target.value],
    //  })
    if(event.target.checked) {
       this.setState({
         skills: [...this.state.skills, event.target.value],
       })
    }
    else {
      let updatedArr = this.state.skills.filter(ele=> ele !== event.target.value);
      this.setState({
        skills: updatedArr,
      })
    }
   }

//   ageChangeHandler = (event) => {
//     this.setState({
//       age: event.target.value,
//     });
//   };

  submitHandler = (event) => {
      event.preventDefault();
      const data = { 
          ...this.state, 
          age: +this.state.age 
        };
      console.log(data);
  }

  render() {
    const { fullName, age, myClass, division, skills } = this.state;
    return (
      <div>
        <form>
          {/* full name ======================= */}
          <div>
            <label htmlFor="fullName">Full Name:</label>
            <input
              type="text"
              name="fullName"
              value={fullName}
              onChange={this.changeHandler}
            />
          </div>
          {/* age ============================== */}
          <div>
            <label htmlFor="age">Age:</label>
            <input
              type="number"
              name="age"
              value={age}
              onChange={this.changeHandler}
            />
          </div>
          {/* class =============================== */}
          <div>
            <label htmlFor="myClass">Select your class</label>
            <select
              name="myClass"
              value={myClass}
              onChange={this.changeHandler}
            >
              <option value="one"> One </option>
              <option value="two">Two</option>
              <option value="three">Three</option>
              <option value="four">Four</option>
              <option value="five">Five</option>
            </select>
          </div>
          {/* division ================================== */}
          <div>
            <label htmlFor="division">Select your Division</label>
            <select
              name="division"
              value={division}
              onChange={this.changeHandler}
            >
              {divisionList.map((ele) => (
                <option key={ele.value} value={ele.value}>
                  {ele.label}
                </option>
              ))}
            </select>
          </div>
          {/* skills =================================== */}
          <div>
            <label htmlFor="">Select Your Skills</label>
            <br />
            <input
              type="checkbox"
              name="skills"
              value='javascript'
              onChange={this.checkboxHandler}
            />
            <label htmlFor="skills">JavaScript</label>
            <br />
            <input
              type="checkbox"
              name="skills"
              value='node'
              onChange={this.checkboxHandler}
            />
            <label htmlFor="skills">Node</label>
            <br />
            <input
              type="checkbox"
              name="skills"
              value='express'
              onChange={this.checkboxHandler}
            />
            <label htmlFor="skills">Express</label>
            <br />
            <input
              type="checkbox"
              name="skills"
              value='react'
              onChange={this.checkboxHandler}
            />
            <label htmlFor="skills">React</label>
          </div>
          {/* submit handler ============================== */}
          <div>
            <button onClick={this.submitHandler}>Submit</button>
          </div>
        </form>
      </div>
    );
  }
}

export default ControlledForm;

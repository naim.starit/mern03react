import React, { Component } from 'react'
import './styles.css';  // css stylesheet
export class ListRenderComp extends Component {
  render() {
      let students = [
        {
          id: 1,
          name: "Next",
          roll: 5,
        },
        {
          id: 2,
          name: "Topper",
          roll: 4,
        },
        {
          id: 3,
          name: "Sakib",
          roll: 7,
        },
        {
          id: 4,
          name: "Tamim",
          roll: 2,
        },
      ];
    return (
      <div>
        <h1>Students</h1>
        {students.map((ele) => (
          <div key={ele.id}>
            <h2>ID: {ele.id}</h2>
            <h2 className='title'>Name: {ele.name}</h2>
            <h2>Roll: {ele.roll}</h2>
            <h2>
              Name: {ele.name}- Roll: {ele.roll}
            </h2>
            <h5>======================</h5>
          </div>
        ))}
      </div>
    );
  }
}
export default ListRenderComp
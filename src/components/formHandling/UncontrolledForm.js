import React, { Component } from 'react'

class UncontrolledForm extends Component {
    constructor () {
        super();
        this.nameInput = React.createRef();
        this.ageInput = React.createRef();
    }
submitHandler = (event) => {
    event.preventDefault();
    let name, age;
    name = this.nameInput.current.value;
    age = this.ageInput.current.value;
    console.log({name, age});
}
  render() {
    return (
      <div>
          <form>
              <input type="text" ref={this.nameInput} />
              <input type="number" ref={this.ageInput} />
              <button onClick={this.submitHandler}>Submit</button>
          </form>
      </div>
    )
  }
}

export default UncontrolledForm;
import React, {useState, useEffect} from 'react'

export const ShowCoordinate = () => {
    const [x, setX] = useState(0);
    const [y, setY] = useState(0);

    function showMousePosition(e) {
      setX(e.clientX);
      setY(e.clientY)
    }

    useEffect(()=> {
      window.addEventListener("mousemove", showMousePosition);
      console.log('hello');

        return ()=> {
            window.removeEventListener("mousemove", showMousePosition);
        }
    }, [])

  return (
    <div>
        <h1>X: {x}, Y: {y}</h1>
    </div>
  )
}

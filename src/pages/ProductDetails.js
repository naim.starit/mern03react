import React from 'react'
import { data } from './Product';
import { useParams } from 'react-router-dom';

export const ProductDetails = () => {
    const params = useParams();
    const productId = params.id;
    const allData = data;

    const productDetails = allData.find(ele=> ele.id === productId);
    const {id, name, img} = productDetails;
  return (
    <div>
      <h1>Product Details</h1>
      <hr />
      <img height="400px" width="700px" src={img} alt="product image"/>
      <h2>Product Id: {id}</h2>
      <h2>Product Name: {name}</h2>
    </div>
  );
}

import React, { Component } from 'react'
import Card from './Card';
import {classSix, class7} from './data';
class ListParentComp extends Component {
  render() {
    return (
      <div>
        <div>
          <h1>Class - 6</h1>
          {classSix.map((ele, ind) => (
            <Card key={ele.id} ele={ele} ind={ind}/>
          ))}
        </div>
        <div>
          <h1>Class - 7</h1>
          {class7.map((ele, ind) => <Card key={ele.id} ele={ele} ind={ind} />)}
        </div>
      </div>
    );
  }
}
export default ListParentComp;
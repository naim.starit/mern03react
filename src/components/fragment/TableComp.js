import React, { Component, Fragment } from 'react'
import ColumnComp from './ColumnComp'

export default class TableComp extends Component {
  render() {
    return (
      <>
        <table>
          <tbody>
            <tr>
              <ColumnComp />
            </tr>
          </tbody>
        </table>
        <div>
            {
                new Array(5).fill("hello").map((ele, ind)=> (
                    <Fragment key={ind}>
                        <h1 style={{color: 'red'}}>{ind}</h1>
                        <h2>{ele}</h2>
                    </Fragment>
                ))
            }
        </div>
      </>
    );
  }
}

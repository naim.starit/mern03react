import React, { Component } from 'react'

export class Count extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      isLoggedin: false,
    };
  }

  increment = () => {
    this.setState((prevState) => ({
      count: prevState.count + 1,
    }));
  };

  incrementFiveHandler = () => {
    this.increment();
    this.increment();
    this.increment();
    this.increment();
    this.increment();
  };

  //=============== login handler
  loginHandler = () => {
    this.setState({
      isLoggedin: true,
    });
  };
  logoutHandler = () => {
    this.setState({
      isLoggedin: false,
    });
  };

  // loginLogoutHandler = () => {
  //   this.setState((prevState)=> ({
  //     isLoggedin: !prevState.isLoggedin
  //   }))
  // }

  render() {
    console.log("render method", this.state.count);
    return (
      <div>
        <h1>Count - {this.state.count}</h1>
        <button onClick={this.incrementFiveHandler}>Increment with 5</button>
        <h1>================================</h1>
        {this.state.isLoggedin ? (
          <button onClick={this.logoutHandler}>Logout</button>
        ) : (
          <button onClick={this.loginHandler}>Login</button>
        )}

        {/* <button onClick={this.loginLogoutHandler}>
          {this.state.isLoggedin ? "Logout" : "Login"}
        </button> */}
      </div>
    );
  }
}

export default Count
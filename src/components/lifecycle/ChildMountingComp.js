// 1. Mounting phase
// 2. Updating phase
// 3. Unmounting phase
// 4. Error handling phase
import axios from "axios";
import React, { Component } from "react";

class ChildMountingComp extends Component {
  constructor() {
         super();
      this.state = {
          loading: true,
          data: []
      }
    console.log("from child ==== constructor");
  }

  static getDerivedStateFromProps(props, state) {
    console.log("from child ====  getDerivedStateFromProps");
    return 0;
  }

  componentDidMount() {
    console.log("from child ====  didMount");
    // setTimeout(()=>{
    //     this.setState({
    //         loading: false,
    //         data: 'This is your data'
    //     })
    // }, 5000)

    axios.get("https://jsonplaceholder.typicode.com/users")
    .then((res)=> {
        console.log('res========', res.data);
        this.setState({
            loading: false,
            data: res.data
        })
    })
    .catch(err => console.log(err))
  }

  render() {
    console.log("from child ====  render");
    return <div>
        {
            this.state.loading ? <h1>...Loading.......</h1> : 
            <h1>{this.state.data.map(ele=> (
                <div key={ele.id}>
                    <h1>Name: {ele.name}</h1>
                    <h1>Phone: {ele.phone}</h1>
                    <h1>Email: {ele.email}</h1>
                    <h2>=============================</h2>
                </div>
            ))}</h1>
        }
    </div>;
  }
}

export default ChildMountingComp;

//===== mounting phase
// constructor()
// static getDerivedStateFromProps()
// render()
// componentDidMount()

import React, { Component } from 'react'
import ProductContext from '../../context/productContext';
import CompC from './CompC';
class CompB extends Component {
  static contextType = ProductContext; // way 1
  render() {
    const { name, age } = this.context;
    return (
      <div>
        <CompC />
        <h1>Comp B ======================</h1>
        <h1>
          {name} - {age}
        </h1>
      </div>
    );
  }
}
// CompB.contextType = ProductContext; //way 2
export default CompB;
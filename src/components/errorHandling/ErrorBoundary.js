import React, { Component } from 'react'

export default class ErrorBoundary extends Component {
    constructor() {
        super();
        this.state = {
            hasError: false
        }
    }
    static getDerivedStateFromError() {
        return {
            hasError: true
        }
    }
  render() {
    if(this.state.hasError) {
        return <h1>Maybe some errors occured</h1>
    }
    else return this.props.children;
  }
}

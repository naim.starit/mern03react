import React from 'react'
const PureJSComp = () => {
  return React.createElement(
    "div",
    null,
    React.createElement("h1", null, "hello from pure js comp")
  );
}
export default PureJSComp;
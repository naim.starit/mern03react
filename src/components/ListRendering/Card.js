import React from 'react'
import styles from './Card.module';
const Card = ({ind, ele}) => {
  return (
    <div style={(ind%2 === 0)? styles.containerBlack : styles.containerGreen}>
      <h2 style={{...styles.text, ...styles.color}}>ID: {ele.id}</h2>
      <h2>Index - {ind}</h2>
      <h2>Name: {ele.name}</h2>
      <h2>Roll: {ele.roll}</h2>
      <h2 style={styles.text}>
        Name: {ele.name}- Roll: {ele.roll}
      </h2>
      <h5>======================</h5>
    </div>
  );
}
export default Card; 


// Css Stylesheet 
// inline style 
// css modulues 
// css in libraries  
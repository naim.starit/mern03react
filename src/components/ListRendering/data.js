export let classSix = [
  {
    id: 1,
    name: "Next",
    roll: 5,
  },
  {
    id: 2,
    name: "Topper",
    roll: 4,
  },
  {
    id: 3,
    name: "Sakib",
    roll: 7,
  },
  {
    id: 4,
    name: "Tamim",
    roll: 2,
  },
];

export let class7 = [
  {
    id: 1,
    name: "Abc",
    roll: 5,
  },
  {
    id: 2,
    name: "Def",
    roll: 4,
  },
  {
    id: 3,
    name: "Rahim",
    roll: 7,
  },
  {
    id: 4,
    name: "Karim",
    roll: 2,
  },
];

export const divisionList = [
  {
    label: "Dhaka",
    value: "Dhaka",
  },
  {
    label: "Rajshahi",
    value: "Rajshahi",
  },
  {
    label: "Chittagong",
    value: "Chittagong",
  },
  {
    label: "Sylhet",
    value: "Sylhet",
  },
];
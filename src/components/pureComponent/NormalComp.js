import React, { Component } from 'react'

export default class NormalComp extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        // if(nextProps.name !== this.props.name) {
        //     return true;
        // }
        if(JSON.stringify(nextProps) !== JSON.stringify(this.props)) {
            return true;
        }
        else return false;
    }

  render() {
      console.log('Normal component');
    return (
      <div>
        <h2>Normal component</h2>
        <h3>Name - {this.props.name} - {this.props.age}</h3>
      </div>
    );
  }
}

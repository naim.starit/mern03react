import React, { Component } from 'react'

export class ConditionalIfElse extends Component {
constructor() {
    super();
    this.state = {
        name: "Next Topper",
        isLoggedin: true
    }
}
  render() {
    if(this.state.isLoggedin) {
        return (
            <div>
                <h1>Hello, {this.state.name}</h1>
            </div>
        )
    }
    else {
        return (
            <div>
                <h1>Please login</h1>
            </div>
        )
    }
  }
}

export default ConditionalIfElse
import React, { Component } from "react";

export class TernaryComp extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      data: "",
    };
  }
fetchDataHandler = () => {
    this.setState({
        loading: true,
        data: "data fetched"
    });
}

loadingOffHandler = () => {
    this.setState({
        loading: false
    })
}

  render() {
     const {loading, data} = this.state;
      return (
          <div>
               { 
                   loading ?
                   <div>
                       <h1>......loading........</h1>
                       <button onClick={this.loadingOffHandler}>Loading Off</button>
                   </div> : 
                   <div>
                       {data && <h1>Your data is here</h1>}
                   </div>
               }
               {!data && <button onClick={this.fetchDataHandler}>Fetch Data</button>}
          </div>
      )
  }
    
}

export default TernaryComp;

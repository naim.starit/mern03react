import React from 'react'

function ChildComp({color, handler, print}) {
  return (
    <div>
        <h1>Hello from Child Comp - {color}</h1>
        <button onClick={()=>handler()}>Child - Chage color</button>
        <button onClick={print}>click</button>
    </div>
  )
}
export default ChildComp
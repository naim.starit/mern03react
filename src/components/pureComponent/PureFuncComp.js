import React from 'react'

function PureFuncComp({name}) {
    console.log("hello func");
  return (
    <div>
        <h1>Pur func comp</h1>
        <h2>{name}</h2>
    </div>
  )
}
export default React.memo(PureFuncComp);
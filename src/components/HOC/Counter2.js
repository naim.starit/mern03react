import React, { Component } from 'react'
import WithCounter from './WithCounter';

class Counter2 extends Component {
  render() {
    return (
      <div>
        <h1>OnMouseOver  - {this.props.myName}</h1>
        <h1>Count - {this.props.count} times</h1>
        <button onMouseOver={this.props.handler}>Increase</button>
      </div>
    );
  }
}

export default WithCounter(Counter2, 10);
import React from 'react'

const Increment = ({handler, children}) => {
    console.log(children);
  return (
    <div>
        <button onClick={handler}>{children}</button>
    </div>
  )
}

export default React.memo(Increment);
import React from 'react'
import styles from './ProductCard.module.css';

const ProductCard = ({ind}) => {
  return (
    <div className={styles.container}>
      <img
        className={styles.image}
        src="https://cdn.mos.cms.futurecdn.net/6t8Zh249QiFmVnkQdCCtHK.jpg"
        alt="product-image"
      />
      <h2 className={styles.title}>ID: Laptop - {ind}</h2>
      <h2 className={styles.title}>{"Laptop G5"}</h2>
      <h4 className={styles.price}>Price:{" $500"}</h4>
    </div>
  );
}

export default ProductCard
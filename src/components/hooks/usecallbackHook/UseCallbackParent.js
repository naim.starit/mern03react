import React, {useState, useCallback} from 'react'
import Increment from './Increment'
import ShowCountValue from './ShowCountValue';

export const UseCallbackParent = () => {
    const [count1, setCount1] = useState(0);
    const [count2, setCount2] = useState(100);

    const countHandler1 = useCallback(() => {
      setCount1(count1 + 1);
    },[count1]);

    const countHandler2 = useCallback(() => {
      setCount2(count2 + 1);
    }, [count2]);

  return (
    <div>
      <ShowCountValue count={count1} > Show count 1</ShowCountValue>
      <Increment handler={countHandler1}>Increment Count 1</Increment>
      <ShowCountValue count={count2} > Show count 2 </ShowCountValue>
      <Increment handler={countHandler2}>Increment Count 2</Increment>
    </div>
  );
}

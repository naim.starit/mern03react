import React, { Component } from 'react'
import UnmountingChild from './UnmountingChild';

class UnmountingCom extends Component {
    constructor() {
        super();
        this.state={
            isVisible: true
        }
    }
    hideHandler = () => {
        this.setState({
            isVisible: false
        })
    }

  render() {
    return (
      <div>
          <h1>Unmounting Component</h1>
          <button onClick={this.hideHandler}>hide</button>
            {
                this.state.isVisible ? <UnmountingChild /> :
                 <h1>I am from Unmounting component</h1>
            }
      </div>
    )
  }
}

export default UnmountingCom;
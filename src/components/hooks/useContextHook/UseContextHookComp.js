import React, {useContext} from 'react'
import AuthContext from '../../../context/authContext'
import ProductContext from '../../../context/productContext';

export const UseContextHookComp = () => {
    let authInfo = useContext(AuthContext);
    let productInfo = useContext(ProductContext);
  return (
    <div>
      <h1>**************************</h1>
      <h2>{authInfo.isLoggedIn}</h2>
      <h2>{productInfo.age}</h2>
    </div>
  );
}

import React, {useContext} from 'react'
import { CountContext } from './ReducerParent';

export const ReducerChildComp = () => {
    const {info, dispatch} = useContext(CountContext)
  return (
    <div>
      <h1>Reducer Child Comp - {info.count}</h1>
      <button onClick={()=>dispatch({type: 'increment', value: 10})}>Increment from Child Comp</button>
    </div>
  );
}

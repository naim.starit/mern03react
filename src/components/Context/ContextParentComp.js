import React, { Component } from 'react'
import { AuthProvider } from '../../context/authContext';
import { ProductProvider } from '../../context/productContext';
import { UseContextHookComp } from '../hooks/useContextHook/UseContextHookComp';
import CompA from './CompA';

export default class ContextParentComp extends Component {
    constructor() {
        super();
        this.state={
            name: "Next Topper",
            age: 10,
            isLoggedIn: false
        }
    }
    loginLogouthandler = () => {
      this.setState(prevState=>(
        {
          isLoggedIn: !prevState.isLoggedIn
        }
      ))
    }
  render() {
    const {name, age, isLoggedIn} =this.state;
    return (
      <ProductProvider value={{ name, age }}>
        <AuthProvider value={{ isLoggedIn, handler: this.loginLogouthandler }}>
          <CompA />
          {/* for useContext Hook */}
          <UseContextHookComp />
        </AuthProvider>
      </ProductProvider>
    );
  }
}

import React, {useReducer} from 'react';
import { ReducerChildComp } from './ReducerChildComp';
export const CountContext = React.createContext();
const initialState = {
    count: 0
}

const reducer = (state, action) => {
    switch(action.type) {
        case 'increment':
            return {count: state.count + action.value};
        case 'decrement':
             return { count: state.count - action.value };
        default: 
            return state;
    }
}

export const ReducerParent = () => {
    const [info, dispatch] = useReducer(reducer, initialState);
  return (
    <CountContext.Provider value = {{info, dispatch}}>
      <h1>Count - {info.count}</h1>
      <button onClick={() => dispatch({ type: "increment", value: 5 })}>
        Increment
      </button>
      <button onClick={() => dispatch({ type: "decrement", value: 2 })}>
        Decrement
      </button>
      <ReducerChildComp />
    </CountContext.Provider>
  );
}

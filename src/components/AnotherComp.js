import React, {Component} from "react";
class AnotherComp extends Component {
    constructor() {
        super();
        this.state = {
            name: "Next",
            age: 2
        }
    }

    changeNameHandler() {
        // this.state.name = "Topper";
        this.setState({
            name: "Topper"
        })
    }
    render() {
        console.log(this.state.name);
        return (
            <div >
                <h1 style={{color: 'green', fontSize: '25px'}}>
                    name: {this.state.name}
                    <br/>
                    age: {this.state.age}
                </h1>
                <button onClick={()=> this.changeNameHandler()}>Change Name</button>
            </div>
        )
    }
}
export default AnotherComp;
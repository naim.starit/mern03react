import React, { Component } from 'react'

export default class UnmountingChild extends Component {
    constructor() {
        super();
        this.state = {
            count: 0
        }
    }
    componentWillUnmount() {
        console.log('from unmount');
    }

    countingHandler = () => {
        setInterval(()=>{
            this.setState(prevState=>(
                {count: prevState.count + 1}
            ))
        }, 1000)
    }
    
  render() {
    return (
      <div>
        <h1>Unmounting child - {this.state.count}</h1>
        <button onClick={this.countingHandler}>Start</button>
      </div>
    );
  }
}

import React, { Component } from "react";

export class IfElseVariableStore extends Component {
  constructor() {
    super();
    this.state = {
      name: "Tamim",
      isLoggedin: false,
    };
  }
  render() {
      let myJsx; 
    if (this.state.isLoggedin) {
     myJsx = <h1>Hello, {this.state.name}</h1>
    } else {
      myJsx = <h1>Please login</h1>
    }
    return (
      <div>
        {myJsx}
      </div>
    );
  }
}

export default IfElseVariableStore;

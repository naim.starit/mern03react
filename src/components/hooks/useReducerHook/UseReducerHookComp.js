import React, {useReducer} from 'react'

const initialState = {
    count1: 0,
    count2: 100
};

const reducer = (state, action) => {
    switch (action.type) {
      case "increment":
        return { ...state, count1: state.count1 + action.value };
      case "decrement":
        return { ...state, count1: state.count1 - action.value };
      case "reset":
        return { ...state, count1: initialState.count1 };
      case "increment1":
        return { ...state, count2: state.count2 + action.value };
      case "decrement1":
        return { ...state, count2: state.count2 - action.value };
      case "reset1":
        return {...state, count2: initialState.count2};
      default:
        return state;
    }
}

export const UseReducerHookComp = () => {
   const [count, dispatch] = useReducer(reducer, initialState);
  return (
    <div>
      <h1>Value: {count.count1} </h1>
      <button onClick={() => dispatch({ type: "increment", value: 5 })}>
        Increment
      </button>
      <button onClick={() => dispatch({ type: "decrement", value: 1 })}>
        Decrement
      </button>
      <button onClick={() => dispatch({ type: "reset" })}>Reset</button>
      {/* ================================== */}
      <h1>{count.count2}</h1>
      <button onClick={() => dispatch({ type: "increment1", value: 10 })}>
        Increment 2
      </button>
      <button onClick={() => dispatch({ type: "decrement1", value: 5 })}>
        Decrement 2
      </button>
      <button onClick={() => dispatch({ type: "reset1" })}>Reset 2</button>
    </div>
  );
}

import React, { Component } from 'react'
import ChildComp from './ChildComp';

class ParentComp extends Component {
    constructor() {
        super();
        this.state = {
            color: "Blue"
        }
    }

    changeColorHandler = () => {
        this.setState({
            color: "Black"
        })
    }

  render() {
    return (
      <div>
          <h1>Parent comp - {this.state.color}</h1>
          <ChildComp color={this.state.color} handler={this.changeColorHandler} print={this.printHandler} /> 
          <button onClick={this.changeColorHandler}>Parent -Change Color</button>
      </div>
    )
  }
}

export default ParentComp;
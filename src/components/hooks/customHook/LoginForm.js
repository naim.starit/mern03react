import React, {useRef, useEffect} from 'react'
import { useInput } from './myHooks/useInput';

export const LoginForm = () => {
  const userNameRef = useRef(null);
 const [userName, resetUserName] = useInput('', "text");
 const [password, resetPassword] = useInput('', "password");

 useEffect(()=>{
  userNameRef.current.focus();
 }, [])

 const submitHandler = () => {
     const data = {
         userName: userName.value,
         password: password.value
     }
     console.log(data);
     resetPassword();
     resetUserName();
 }
  return (
    <div>
      <label>User Name:</label>
      <input ref={userNameRef} {...userName} />
      <br />
      <label>Password:</label>
      <input {...password}/>
       <button onClick={submitHandler}>Submit</button>
    </div>
  );
}

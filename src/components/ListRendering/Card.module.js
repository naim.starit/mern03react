const styles = {
    containerGreen: {
        width: '500px',
        height: '350px',
        backgroundColor: 'green'
    },
    containerBlack: {
        width: '500px',
        height: '350px',
        backgroundColor: 'black'
    },
    text: {
        fontSize: '30px'
    },
    color: {
        color: '#ffffff'
    }
};

export default styles;
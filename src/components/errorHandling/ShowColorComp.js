import React from 'react'

export const ShowColorComp = ({color}) => {
    if(color === 'blue') {
        throw new Error('This color is not valid')
    }
  return (
    <div>
        <h1>Your color is: {color}</h1>
    </div>
  )
}

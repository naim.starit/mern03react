import React, {useState, useEffect} from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';

export const data = [
  {
    id: "1",
    category: "mobile",
    name: "Mobile",
    img: "https://static.digit.in/product/thumb_198189_product_td_300.jpeg",
  },
  {
    id: "2",
    category: "laptop",
    name: "Laptop",
    img: "https://cdn.mos.cms.futurecdn.net/6t8Zh249QiFmVnkQdCCtHK.jpg",
  },
  {
    id: "3",
    category: "laptop",
    name: "Macbook mini",
    img: "https://cdn.vox-cdn.com/thumbor/vGTTyiMMUHIHLs0FjoF0z99m6_E=/0x0:2040x1360/1200x800/filters:focal(895x304:1221x630)/cdn.vox-cdn.com/uploads/chorus_image/image/68592461/vpavic_4291_20201113_0366.0.0.jpg",
  },
];

export const Product = () => {
   const navigate = useNavigate();
   const [params, setParams] = useSearchParams();
   const [myData, setMyData] = useState(data);
   const filterCategory = params.get('category');

   useEffect(() => {
      if(filterCategory) {
        const filteredData = data.filter(ele=> ele.category === filterCategory);
        setMyData(filteredData)
      }
   }, [filterCategory]);
   console.log(myData);
  return (
    <div>
        <h1>All products</h1>
        <button onClick={()=> setParams({category: 'laptop'})}>All laptop</button>
        <button onClick={()=> setParams({category: 'mobile'})}>All mobile</button>
        <button onClick={()=> setMyData(data)} >All product</button>
        {
            myData.map(ele=> (
                <React.Fragment key={ele.id}>
                <h2>Product Id: {ele.id}</h2>
                <h2>Product Name: {ele.name}</h2>
                <button onClick={()=>navigate(`details/${ele.id}`)}>View Details</button>
                <br />
                </React.Fragment>
            ))
        }
    </div>
  )
}

import React, { Component } from 'react'
import NormalComp from './NormalComp'
import PureComp from './PureComp'
import PureFuncComp from './PureFuncComp';

export default class PureParentComp extends Component {
    constructor() {
        super();
        this.state = {
            name: "Next",
            age: 12,
        }
    }

    componentDidMount(){
        setInterval(()=>{
            this.setState({
                name: "Topper",
                age: 15
            })
        }, 1000)
    }
  render() {
      console.log('parent=========');
    return (
      <div>
        <h1>Parent Component</h1>
        <PureComp name={this.state.name} />
        <NormalComp name={this.state.name} age={this.state.age} />
        <PureFuncComp name={this.state.name} />
      </div>
    );
  }
}

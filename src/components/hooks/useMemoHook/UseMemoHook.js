import React, {useState, useMemo} from 'react'

export const UseMemoHook = () => {
const [count1, setCount1] = useState(0);
const [count2, setCount2] = useState(0);

const handler1 = () => {
    setCount1(count1+1)
}
const handler2 = () => {
    setCount2(count2+1)
}

const calculate = useMemo(() => {
  let i = 0;
  while (i < 1000000000) i++;
  return count1 + 10;
}, [count1]);

  return (
    <div>
        <h2>Calculate value: {calculate}</h2>
      <h1>{count1}</h1>
      <button onClick={handler1}>Increase Count 1</button>
      <br />
      <h1>{count2}</h1>
      <button onClick={handler2}>Increase Count 2</button>
    </div>
  );
}

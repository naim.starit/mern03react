import React, { PureComponent } from 'react'

export default class PureComp extends PureComponent {
  render() {
    console.log("pure component");
    return (
      <div>
        <h2>Pure component</h2>
        <h3>Name - {this.props.name}</h3>
      </div>
    );
  }
}

import React from "react";
import {useNavigate} from 'react-router-dom'

export const Contact = () => {
    const navigate = useNavigate()
  return (
    <div>
      <h1>This is Contact page</h1>
      <button onClick={()=> navigate(-1)}>Go to back page</button>
    </div>
  );
};

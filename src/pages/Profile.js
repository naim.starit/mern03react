import React from 'react'
import { Link } from 'react-router-dom'

export const Profile = ({logout}) => {
  return (
    <div>
        <h1>This is your profile page</h1>
        <Link to="/dashboard">Dashboard</Link>
        <br />
        <button onClick={logout}>Logout</button>
    </div>
  )
}

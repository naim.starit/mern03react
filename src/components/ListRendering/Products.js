import React from 'react'
import ProductCard from './ProductCard'

const Products = () => {
  return (
    <div style={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
        {
            new Array(10).fill('0').map((ele, index)=> <ProductCard key={index} ind={index} />)
        } 
    </div>
  )
}

export default Products
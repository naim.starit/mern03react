import React, {Component} from 'react';

const WithCounter = (ReceivedComp, value) => {
    class NewComp extends Component {
      constructor() {
        super();
        this.state = {
          name: "Next",
          count: 0
        };
      }
      increamentHandler = () => {
        this.setState((prevState) => ({
          count: prevState.count + value,
        }));
      };
      render() {
        return <ReceivedComp 
            name={this.state.name} 
            count={this.state.count} 
            handler = {this.increamentHandler}
            {...this.props}
        />;
      }
    }
    return NewComp;
}

export default WithCounter;
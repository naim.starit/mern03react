import React from 'react'

const ShowCountValue = ({count, children}) => {
    console.log(children);
  return (
    <div>
        <h1>{count}</h1>
    </div>
  )
}

export default React.memo(ShowCountValue);
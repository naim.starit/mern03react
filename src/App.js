import { Navigate, Route, Routes } from 'react-router-dom';
import {useState, useEffect} from 'react';
import './App.css';
import AnotherComp from './components/AnotherComp';
import ConditionalIfElse from './components/conditionalRendering/ConditionalIfElse';
import IfElseVariableStore from './components/conditionalRendering/IfElseVariableStore';
import TernaryComp from './components/conditionalRendering/TernaryComp';
import ContextParentComp from './components/Context/ContextParentComp';
import ErrorParentComp from './components/errorHandling/ErrorParentComp';
import ControlledForm from './components/formHandling/ControlledForm';
import UncontrolledForm from './components/formHandling/UncontrolledForm';
import TableComp from './components/fragment/TableComp';
import Hello from './components/Hello';
import Counter1 from './components/HOC/Counter1';
import Counter2 from './components/HOC/Counter2';
import { LoginForm } from './components/hooks/customHook/LoginForm';
import { UseCallbackParent } from './components/hooks/usecallbackHook/UseCallbackParent';
import { UseEffectHook } from './components/hooks/useEffectHook/UseEffectHook';
import { UseMemoHook } from './components/hooks/useMemoHook/UseMemoHook';
import { ReducerDataFetch } from './components/hooks/useReducerHook/ReducerAndContext/ReducerDataFetch';
import { ReducerParent } from './components/hooks/useReducerHook/ReducerAndContext/ReducerParent';
import { UseReducerHookComp } from './components/hooks/useReducerHook/UseReducerHookComp';
import UseStateHook from './components/hooks/useStateHook.js/UseStateHook';
import MountingComp from './components/lifecycle/MountingComp';
import UnmountingCom from './components/lifecycle/UnmountingCom';
import UpdatingComp from './components/lifecycle/UpdatingComp';
import ListParentComp from './components/ListRendering/ListParentComp';
import ListRenderComp from './components/ListRendering/ListRenderComp';
import Products from './components/ListRendering/Products';
import ParentComp from './components/ParentComp';
import Count from './components/prevState/Count';
import PureParentComp from './components/pureComponent/PureParentComp';
import PureJSComp from './components/PureJSComp';
import { About } from './pages/About';
import {Login } from './pages/Login';
import { Contact } from './pages/Contact';
import { Dashboard } from './pages/Dashboard';
import { Home } from './pages/Home';
import { LatestProduct } from './pages/LatestProduct';
import { NavBar } from './pages/NavBar';
import { NotFound } from './pages/NotFound';
import { Product } from './pages/Product';
import { ProductDetails } from './pages/ProductDetails';
import { Profile } from './pages/Profile';
import { UpcommingProduct } from './pages/UpcommingProduct';

function App() {
  const [user, setUser] = useState(null);

  useEffect(()=> {
    const userInfo = localStorage.getItem('user');
    userInfo && JSON.parse(userInfo) ? setUser(true) : setUser(false)
  }, [])

  useEffect(()=> {
    localStorage.setItem('user', user)
  }, [user])
  return (
    <div className="App">
      {/* <Hello name="Rakib" age={10} /> 
      <Hello /> 
      <Hello name="Tamim" age={20}/>  */}
      {/* <Hello name="Tamim" age={20}>
        <h2 style={{ color: "red" }}>Hello from h2 tag</h2>
        <h1>Hello</h1>
      </Hello> */}
      {/* <AnotherComp name="Rakib" age={10} /> */}
      {/* <AnotherComp /> */}
      {/* <PureJSComp /> */}
      {/* <ParentComp /> */}
      {/* ==================================== */}
      {/* <ConditionalIfElse /> */}
      {/* <TernaryComp /> */}
      {/* <IfElseVariableStore /> */}
      {/* ===================================== */}
      {/* <Count /> */}
      {/* <ListRenderComp /> */}
      {/* <Products /> */}
      {/* <ListParentComp /> */}
      {/* <ControlledForm /> */}
      {/* <UncontrolledForm /> */}
      {/* <MountingComp /> */}
      {/* <UpdatingComp /> */}
      {/* <UnmountingCom /> */}
      {/* <TableComp /> */}
      {/* <PureParentComp /> */}
      {/* <ErrorParentComp /> */}
      {/* <Counter1 age={10} />
      <Counter2 myName="Hello" /> */}
      {/* <ContextParentComp /> */}
      {/* <UseStateHook /> */}
      {/* <UseEffectHook /> */}
      {/* <UseReducerHookComp /> */}
      {/* <ReducerParent /> */}
      {/* <ReducerDataFetch /> */}
      {/* <LoginForm /> */}
      {/* <UseCallbackParent /> */}
      {/* <UseMemoHook /> */}
      <NavBar />
      <Routes>
        {!user && (
          <>
            <Route path="/" element={<Home />} />
            <Route path="about" element={<About />} />
            <Route path="contact" element={<Contact />} />
            <Route path="product">
              <Route index element={<Product />} />
              <Route path="latest" element={<LatestProduct />}></Route>
              <Route path="upcomming" element={<UpcommingProduct />}></Route>
              <Route path="details/:id" element={<ProductDetails />} />
            </Route>
            <Route
              path="login"
              element={<Login authenticate={() => setUser(true)} />}
            />
          </>
        )}

        {user && (
          <>
            <Route
              path="profile"
              element={<Profile logout={() => setUser(false)} />}
            />
            <Route path="dashboard" element={<Dashboard />} />
          </>
        )} 
        <Route path="*" element={<Navigate to={user? '/profile' : '/login'} />} />
      </Routes>
    </div>
  );
}

export default App;

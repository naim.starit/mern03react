import React, { Component } from 'react'
import WithCounter from './WithCounter';

class Counter1 extends Component {
  render() {
      console.log(this.props);
    return (
      <div>
        <h1>Onclick -  {this.props.age}</h1>
        <h1>Count - {this.props.count} times</h1>
        <button onClick={this.props.handler}>Increase</button>
      </div>
    );
  }
}
export default WithCounter(Counter1, 5);
import React, { Component } from 'react'
import ErrorBoundary from './ErrorBoundary';
import { ShowColorComp } from './ShowColorComp'

export default class ErrorParentComp extends Component {
  render() {
    return (
      <div>
        <ErrorBoundary>
          <ShowColorComp color="green" />
        </ErrorBoundary>
            <ErrorBoundary>
                <ShowColorComp color="black" />
           </ErrorBoundary>
            <ErrorBoundary>
                <ShowColorComp color="blue" />
        </ErrorBoundary>
      </div>
    );
  }
}

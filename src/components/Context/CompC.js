import React from "react";
import { AuthConsumer } from "../../context/authContext";
import { ProductConsumer } from "../../context/productContext";

const CompC = () => {
  return (
    <ProductConsumer>
      {({ name, age }) => (
          <AuthConsumer>
            {
              ({isLoggedIn, handler})=>{
                        return (
                          <div>
                            <h1>Component C</h1>
                            {isLoggedIn ? (
                              <>
                                <h1>
                                  {age} - {name}
                                </h1>
                                <button onClick={handler}>Logout</button>
                              </>
                            ) : (
                              <button onClick={handler}>Login</button>
                            )}
                          </div>
                        );
              }
            }
          </AuthConsumer>
  )}
    </ProductConsumer>
  );
};
export default CompC;

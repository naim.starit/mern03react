// 1. Mounting phase
// 2. Updating phase
// 3. Unmounting phase
// 4. Error handling phase
import React, { Component } from 'react';
import ChildMountingComp from './ChildMountingComp';

class MountingComp extends Component {
  constructor() {
    super();
    console.log("from constructor");
  }

  static getDerivedStateFromProps(props, state) {
    console.log("from getDerivedStateFromProps");
    return 0;
  }

    componentDidMount() {
        console.log('from didMount');
    }

  render() {
      console.log('from render');
    return <div>
        MountingComp
        <ChildMountingComp />
        </div>;
  }
}

export default MountingComp;

//===== mounting phase 
// constructor()
// static getDerivedStateFromProps()
// render()
// componentDidMount()